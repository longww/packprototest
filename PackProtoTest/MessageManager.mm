//
//  MessageManager.m
//  testlib
//
//  Created by longw on 15/1/12.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#import "MessageManager.h"
#import "ConfigDefine.h"


@implementation MessageManager

- (void) initManagerWithUserID : (NSString *) _userId
                    withAppID : (NSString *) _appId
                withAppPassWD : (NSString *) _appPassWD{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:_userId forKey:USER_ID_STR];
    [userDefaults setObject:_appId forKey:APP_ID_STR];
    [userDefaults setObject:_appPassWD forKey:APP_PASSWD_STR];
    [userDefaults synchronize];   
}

- (void) packTest{
    DataResolution dataResolution;
    dataResolution.InitAssistInfo();
    dataResolution.InitTransPkgHeadEx();
    dataResolution.InitDBPackHead();
    dataResolution.pack();


}
@end
