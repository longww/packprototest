//
//  SendingDataBuilder.mm
//  testlib
//
//  Created by longw on 15/1/12.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#include "SendingDataBuilder.h"
#include <stdlib.h>
#include <stdint.h>
#include <netinet/in.h>
#include <string.h>

int GetINT8_V06(char **p, int *piLen, uint8_t *Value)
{
    int iLen;
    char *pCur;
    char *cValue = (char *)Value;
    
    pCur = *p;
    if (piLen) iLen = *piLen;
    else iLen = sizeof(char);
    
    if (iLen < (signed int)sizeof(char)) return -1;
    
    *cValue = *pCur;
    
    pCur += sizeof(char);
    iLen -= sizeof(char);
    
    *p = pCur;
    if (piLen) *piLen = iLen;
    
    return 0;
    //return GetChar(p, piLen, (char *)Value);
}

int AddINT8_V06(char **p, int *piLenLeft, uint8_t Value)
{
    int iLenLeft = 100;
    char *pCur;
    char cValue = (char)Value;
    
    pCur = *p;
    if (piLenLeft) iLenLeft = *piLenLeft;
    
    if (iLenLeft < (signed int)sizeof(cValue)) return -1;
    
    *pCur = cValue;
    pCur += sizeof(cValue);
    iLenLeft -= sizeof(cValue);
    
    *p = pCur;
    if (piLenLeft) *piLenLeft = iLenLeft;
    
    return 0;
    //return AddChar(p, piLenLeft, (char)Value);
}

int GetINT16_V06(char **p, int *piLen, uint16_t *pshValue)
{
    uint16_t shNValue;
    int iLen;
    char *pCur;
    
    pCur = *p;
    if (piLen) iLen = *piLen;
    else iLen = sizeof(short);
    
    if (iLen < (signed int)sizeof(shNValue)) return -1;
    
    memcpy(&shNValue, pCur, sizeof(shNValue));
    *pshValue = ntohs(shNValue);
    pCur += sizeof(shNValue);
    iLen -= sizeof(shNValue);
    
    *p = pCur;
    if (piLen) *piLen = iLen;
    
    return 0;
    //return GetWord(p, piLen, Value);
}

int AddINT16_V06(char **p, int *piLenLeft, uint16_t shValue)
{
    int iLenLeft = 100;
    char *pCur;
    uint16_t shNValue;
    
    pCur = *p;
    if (piLenLeft) iLenLeft = *piLenLeft;
    
    if (iLenLeft < (signed int)sizeof(shValue)) return -1;
    
    shNValue = htons(shValue);
    memcpy(pCur, &shNValue, sizeof(shNValue));
    pCur += sizeof(shValue);
    iLenLeft -= sizeof(shValue);
    
    *p = pCur;
    if (piLenLeft) *piLenLeft = iLenLeft;
    
    return 0;
    //return AddWord(p, piLenLeft, Value);
}

int GetINT32_V06(char **p, int *piLen, uint32_t *plValue)
{
    uint32_t lNValue;
    int iLen;
    char *pCur;
    
    pCur = *p;
    if (piLen) iLen = *piLen;
    else iLen = sizeof(long);
    
    if (iLen < (signed int)sizeof(lNValue)) return -1;
    
    memcpy(&lNValue, pCur, sizeof(lNValue));
    *plValue = ntohl(lNValue);
    pCur += sizeof(lNValue);
    iLen -= sizeof(lNValue);
    
    *p = pCur;
    if (piLen) *piLen = iLen;
    
    return 0;
    //return GetDWord(p, piLen, Value);
}

int AddINT32_V06(char **p, int *piLenLeft, uint32_t lValue)
{
    int iLenLeft = 100;
    char *pCur;
    uint32_t lNValue;
    
    pCur = *p;
    if (piLenLeft) iLenLeft = *piLenLeft;
    
    if (iLenLeft < (signed int)sizeof(lValue)) return -1;
    
    lNValue = htonl(lValue);
    memcpy(pCur, &lNValue, sizeof(lNValue));
    pCur += sizeof(lValue);
    iLenLeft -= sizeof(lValue);
    
    *p = pCur;
    if (piLenLeft) *piLenLeft = iLenLeft;
    
    return 0;
    //return AddDWord(p, piLenLeft, Value);
}


#define swap_ddword(x)  \
((((x) & 0xff00000000000000llu) >> 56) | \
(((x) & 0x00ff000000000000llu) >> 40) | \
(((x) & 0x0000ff0000000000llu) >> 24) | \
(((x) & 0x0000ff0000000000llu) >> 24) | \
(((x) & 0x000000ff00000000llu) >> 8) | \
(((x) & 0x00000000ff000000llu) << 8) | \
(((x) & 0x0000000000ff0000llu) << 24) | \
(((x) & 0x000000000000ff00llu) << 40) | \
(((x) & 0x00000000000000ffllu) << 56) )

uint64_t Switch_V06(uint64_t a)
{
#if 0
    uint64_t iRet;
    int i;
    const unsigned char *psrc = NULL;
    unsigned char *pdst = NULL;
#endif
    
#if BYTE_ORDER == BIG_ENDIAN
    return a;
#elif BYTE_ORDER == LITTLE_ENDIAN
#if 0
    psrc = (const unsigned char *)&a;
    pdst = (unsigned char *)&iRet;
    for (i = 0; i < 8; ++i)
    {
        pdst[i] = psrc[7-i];
    }
    return iRet;
#else
    return swap_ddword(a);
#endif
#else
#error "What kind of system is this?"
#endif
}

int GetINT64_V06(char **ppCur, int *piLeftLen, uint64_t *pval)
{
    if (ppCur == NULL || pval == NULL)
    {
        return -1;
    }
    if (piLeftLen != NULL &&
        (*piLeftLen < 0 || *piLeftLen < sizeof(uint64_t)))
    {
        return -2;
    }
    *pval = *(uint64_t*)(*ppCur);
    *ppCur = *ppCur + sizeof(uint64_t);
    if (piLeftLen)
    {
        *piLeftLen = *piLeftLen - sizeof(uint64_t);
    }
    *pval = ntohll(*pval);
    return 0;
    //return GetDDWord(p, piLen, Value);
}

int AddINT64_V06(char **ppCur, int *piLeftLen, uint64_t val)
{
    if (ppCur == NULL )
    {
        return -1;
    }
    if (piLeftLen != NULL &&
        (*piLeftLen < 0 || *piLeftLen < sizeof(uint64_t)))
    {
        return -2;
    }
    *(uint64_t*)(*ppCur) = htonll(val);
    *ppCur = *ppCur + sizeof(uint64_t);
    if (piLeftLen)
    {
        *piLeftLen = *piLeftLen - sizeof(uint64_t);
    }
    return 0;
    //return AddDDWord(p, piLenLeft, Value);
}

int GetINT8Array_zero_V06(char **p, int *piLen, uint8_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT8_V06(p, piLen, Buf + (i++))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT8Array_zero_V06(char **p, int *piLenLeft, uint8_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(AddINT8_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT16Array_zero_V06(char **p, int *piLen, uint16_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT16_V06(p, piLen, Buf + (i++))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT16Array_zero_V06(char **p, int *piLenLeft, uint16_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(AddINT16_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT32Array_zero_V06(char **p, int *piLen, uint32_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT32_V06(p, piLen, Buf + (i++))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT32Array_zero_V06(char **p, int *piLenLeft, uint32_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(AddINT32_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT64Array_zero_V06(char **p, int *piLen, uint64_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT64_V06(p, piLen, Buf + (i++))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT64Array_zero_V06(char **p, int *piLenLeft, uint64_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(AddINT64_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
        if(!Buf[i-1])return 0;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT8Array_split_V06(char **p, int *piLen, uint8_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT8_V06(p, piLen, Buf + (i++))) return -2;
        if(Buf[i-1] == spliter)
        {
            Buf[i-1] = '\0';
            return 0;
        }
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT8Array_split_V06(char **p, int *piLenLeft, uint8_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(!Buf[i])
        {
            if(AddINT8_V06(p, piLenLeft, spliter)) return -2;
            return 0;
        }
        if(AddINT8_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT16Array_split_V06(char **p, int *piLen, uint16_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT16_V06(p, piLen, Buf + (i++))) return -2;
        if(Buf[i-1] == spliter)
        {
            Buf[i-1] = '\0';
            return 0;
        }
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT16Array_split_V06(char **p, int *piLenLeft, uint16_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(!Buf[i])
        {
            if(AddINT16_V06(p, piLenLeft, spliter)) return -2;
            return 0;
        }
        if(AddINT16_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT32Array_split_V06(char **p, int *piLen, uint32_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT32_V06(p, piLen, Buf + (i++))) return -2;
        if(Buf[i-1] == spliter)
        {
            Buf[i-1] = '\0';
            return 0;
        }
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT32Array_split_V06(char **p, int *piLenLeft, uint32_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(!Buf[i])
        {
            if(AddINT32_V06(p, piLenLeft, spliter)) return -2;
            return 0;
        }
        if(AddINT32_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT64Array_split_V06(char **p, int *piLen, uint64_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT64_V06(p, piLen, Buf + (i++))) return -2;
        if(Buf[i-1] == spliter)
        {
            Buf[i-1] = '\0';
            return 0;
        }
    }
    if(*piLen <= 0) return -2;
    else return -3;
}

int AddINT64Array_split_V06(char **p, int *piLenLeft, uint64_t *Buf, int iBufLen, char spliter)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen)
    {
        if(!Buf[i])
        {
            if(AddINT64_V06(p, piLenLeft, spliter)) return -2;
            return 0;
        }
        if(AddINT64_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT8Array_end_V06(char **p, int *piLen, uint8_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT8_V06(p, piLen, Buf + (i++))) return -2;
    }
    if(*piLen == 0) return 0;
    else if(*piLen < 0) return -2;
    else return -3;
}

int AddINT8Array_end_V06(char **p, int *piLenLeft, uint8_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen && Buf[i] != 0)
    {
        if(AddINT8_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(!Buf[i])return 0;
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT16Array_end_V06(char **p, int *piLen, uint16_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT16_V06(p, piLen, Buf + (i++))) return -2;
    }
    if(*piLen == 0) return 0;
    else if(*piLen < 0) return -2;
    else return -3;
}

int AddINT16Array_end_V06(char **p, int *piLenLeft, uint16_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen && Buf[i] != 0)
    {
        if(AddINT16_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(!Buf[i])return 0;
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT32Array_end_V06(char **p, int *piLen, uint32_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT32_V06(p, piLen, Buf + (i++))) return -2;
    }
    if(*piLen == 0) return 0;
    else if(*piLen < 0) return -2;
    else return -3;
}

int AddINT32Array_end_V06(char **p, int *piLenLeft, uint32_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen && Buf[i] != 0)
    {
        if(AddINT32_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(!Buf[i])return 0;
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

int GetINT64Array_end_V06(char **p, int *piLen, uint64_t *Buf, int iBufLen)
{
    if(p == NULL || piLen == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLen > 0 && i < iBufLen)
    {
        if(GetINT64_V06(p, piLen, Buf + (i++))) return -2;
    }
    if(*piLen == 0) return 0;
    else if(*piLen < 0) return -2;
    else return -3;
}

int AddINT64Array_end_V06(char **p, int *piLenLeft, uint64_t *Buf, int iBufLen)
{
    if(p == NULL || piLenLeft == NULL || Buf  == NULL)return -1;
    int i = 0;
    while(*piLenLeft > 0 && i < iBufLen && Buf[i] != 0)
    {
        if(AddINT64_V06(p, piLenLeft, *(Buf + (i++)))) return -2;
    }
    if(!Buf[i]) return 0;
    if(*piLenLeft <= 0) return -2;
    else return -3;
}

/*********************************************************************************************/
int AddDBPkgHead_V06(char **p, int *piLen, DBPackHead *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wHeadLen))return -1;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wVersion))return -1;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wSubVersion))return -1;
    if(AddINT32_V06(&ps, &iLen, (uint32_t)pst->dwCommand))return -1;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wSeqNum))return -1;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->dwFlag))return -1;
    *p = ps;
    *piLen = iLen;
    return 0;
}

int AddAssistInfo_V06(char **p, int *piLen, AssistInfo *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wInfoLen))return -1;
    if(AddINT32_V06(&ps, &iLen, (uint32_t)pst->dwUserId))return -1;
    if(AddINT32_V06(&ps, &iLen, (uint32_t)pst->dwAppId))return -1;
    if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cAppPassWDLen))return -1;
    {
        int i;
        for (i = 0; i < pst->cAppPassWDLen; i++){
            if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cAppPassWD[i]))return -1;
        }
    }
    if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cNetworkType))return -1;
    if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cDeviceType))return -1;
    if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cDeviceNoLen))return -1;
    {
        int i;
        for (i = 0; i < pst->cDeviceNoLen; i++){
            if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cDeviceNo[i]))return -1;
        }
    }
    if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cDeviceDescLen))return -1;
    {
        int i;
        for (i = 0; i < pst->cDeviceDescLen; i++){
            if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cDeviceDesc[i]))return -1;
        }
    }
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wLocalId))return -1;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wTimeZone))return -1;
    if(AddINT32_V06(&ps, &iLen, (uint32_t)pst->dwTime))return -1;
    if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cFrom))return -1;
    *p = ps;
    *piLen = iLen;
    return 0;
}
int AddTransPkgHeadEx_V06(char **p, int *piLen, TransPkgHeadEx *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wExVer))return -1;
    if(AddINT16_V06(&ps, &iLen, (uint16_t)pst->wExLen))return -1;
    {
        int i;
        for (i = 0; i < pst->wExLen; i++){
            if(AddINT8_V06(&ps, &iLen, (uint8_t)pst->cReserved[i]))return -1;
        }
    }
    *p = ps;
    *piLen = iLen;
    return 0;
}

int AddTransPkgReqHead_V06(char **p, int *piLen, TransPkgReqHead *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(AddDBPkgHead_V06(&ps, &iLen, &pst->stDbHead))return -1;
    if(AddAssistInfo_V06(&ps, &iLen, &pst->stAssistHead))return -1;
    if(AddTransPkgHeadEx_V06(&ps, &iLen, &pst->stTransExHead))return -1;
    *p = ps;
    *piLen = iLen;
    return 0;
}

int AddTransPkgRspHead_V06(char **p, int *piLen, TransPkgRspHead *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(AddDBPkgHead_V06(&ps, &iLen, &pst->stDbHead))return -1;
    if(AddTransPkgHeadEx_V06(&ps, &iLen, &pst->stTransExHead))return -1;
    *p = ps;
    *piLen = iLen;
    return 0;
}

int GetDBPkgHead_V06(char **p, int *piLen, DBPackHead *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wHeadLen))return -1;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wVersion))return -1;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wSubVersion))return -1;
    if(GetINT32_V06(&ps, &iLen, (uint32_t *)&pst->dwCommand))return -1;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wSeqNum))return -1;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->dwFlag))return -1;
    *p = ps;
    *piLen = iLen;
    return 0;
}

int GetAssistInfo_V06(char **p, int *piLen, AssistInfo *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wInfoLen))return -1;
    if(GetINT32_V06(&ps, &iLen, (uint32_t *)&pst->dwUserId))return -1;
    if(GetINT32_V06(&ps, &iLen, (uint32_t *)&pst->dwAppId))return -1;
    if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cAppPassWDLen))return -1;
    {
        int i;
        for (i = 0; i < pst->cAppPassWDLen; i++){
            if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cAppPassWD[i]))return -1;
        }
    }
    if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cNetworkType))return -1;
    if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cDeviceType))return -1;
    if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cDeviceNoLen))return -1;
    {
        int i;
        for (i = 0; i < pst->cDeviceNoLen; i++){
            if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cDeviceNo[i]))return -1;
        }
    }
    if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cDeviceDescLen))return -1;
    {
        int i;
        for (i = 0; i < pst->cDeviceDescLen; i++){
            if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cDeviceDesc[i]))return -1;
        }
    }
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wLocalId))return -1;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wTimeZone))return -1;
    if(GetINT32_V06(&ps, &iLen, (uint32_t *)&pst->dwTime))return -1;
    if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cFrom))return -1;
    *p = ps;
    *piLen = iLen;
    return 0;
}

int GetTransPkgHeadEx_V06(char **p, int *piLen, TransPkgHeadEx *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(GetINT16_V06(&ps, &iLen, (uint16_t *)&pst->wExVer))return -1;
    if(GetINT32_V06(&ps, &iLen, (uint32_t *)&pst->wExLen))return -1;;
    {
        int i;
        for (i = 0; i < pst->wExLen; i++){
            if(GetINT8_V06(&ps, &iLen, (uint8_t *)&pst->cReserved[i]))return -1;
        }
    }
    *p = ps;
    *piLen = iLen;
    return 0;
}
int GetTransPkgReqHead_V06(char **p, int *piLen, TransPkgReqHead *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(GetDBPkgHead_V06(&ps, &iLen, &pst->stDbHead))return -2;
    if(GetAssistInfo_V06(&ps, &iLen, &pst->stAssistHead))return -3;
    if(GetTransPkgHeadEx_V06(&ps, &iLen, &pst->stTransExHead))return -4;
    *p = ps;
    *piLen = iLen;
    return 0;
}

int GetTransPkgRspHead_V06(char **p, int *piLen, TransPkgRspHead *pst)
{
    char *ps = NULL;
    int iLen = 0;
    if(p == NULL || piLen == NULL || pst  == NULL)return -1;
    ps = *p;
    iLen = *piLen;
    if(GetDBPkgHead_V06(&ps, &iLen, &pst->stDbHead))return -2;
    if(GetTransPkgHeadEx_V06(&ps, &iLen, &pst->stTransExHead))return -4;
    *p = ps;
    *piLen = iLen;
    return 0;
}
