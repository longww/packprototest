//
//  ViewController.m
//  PackProtoTest
//
//  Created by longw on 15/1/14.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#import "ViewController.h"
#import "MessageManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    MessageManager * messageManager = [[MessageManager alloc] init];
    [messageManager initManagerWithUserID:@"260" withAppID:@"207" withAppPassWD:@"147483647"];
    [messageManager packTest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
