//
//  DataBuilder.mm
//  testlib
//
//  Created by longw on 15/1/13.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#include "DataBuilder.h"

/**
 *  填充AssistInfo()结构体
 *
 */
bool DataResolution::InitAssistInfo(){
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];

    NSInteger _userIDInt = [[userDefaultes stringForKey:USER_ID_STR] integerValue];
    memcpy(&m_trans_head.stAssistHead.dwUserId, &_userIDInt, sizeof(m_trans_head.stAssistHead.dwUserId));
    
    NSInteger _appIDInt = [[userDefaultes stringForKey:APP_ID_STR] integerValue];
    memcpy(&m_trans_head.stAssistHead.dwAppId, &_appIDInt, sizeof(m_trans_head.stAssistHead.dwAppId));
    
    NSData * _appPassWD = [[userDefaultes stringForKey:APP_PASSWD_STR]  dataUsingEncoding:NSUTF8StringEncoding];
    Byte * _appPassWDBytes = (Byte *)[_appPassWD bytes];
    m_trans_head.stAssistHead.cAppPassWDLen = sizeof(_appPassWDBytes);
    for (int i = 0; i < sizeof(_appPassWDBytes); i++) {
        m_trans_head.stAssistHead.cAppPassWD[i] = _appPassWDBytes[i];
    }
    
    //或者直接memcpy
    //memcpy((char *)m_trans_head.stAssistHead.cAppPassWD, [[userDefaultes stringForKey:APP_PASSWD_STR] UTF8String], [[userDefaultes stringForKey:APP_PASSWD_STR] length]);
    
   
   
    printf("The passwd is %s\n", m_trans_head.stAssistHead.cAppPassWD);
    printf("The passwd len is %d\n", m_trans_head.stAssistHead.cAppPassWDLen);
    
    m_trans_head.stAssistHead.cAppPassWDLen = NETWORK_TYPE_WIFI;
    
    m_trans_head.stAssistHead.cDeviceType = DEVICE_TYPE_IPHONE;
    
    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
   
    NSData * _identifierData= [identifierForVendor dataUsingEncoding:NSUTF8StringEncoding];
    Byte * _identifierBytes = (Byte *)[_identifierData bytes];
    
    m_trans_head.stAssistHead.cDeviceNoLen = sizeof(_identifierBytes);
    for (int i = 0; i < sizeof(_identifierBytes); i++) {
        m_trans_head.stAssistHead.cDeviceNo[i] = _identifierBytes[i];
    }
    
    m_trans_head.stAssistHead.cDeviceDescLen = 0;
    
    m_trans_head.stAssistHead.wLocalId = 0;
    
    m_trans_head.stAssistHead.wTimeZone = 0;
    
    m_trans_head.stAssistHead.dwTime = 0;
    
    m_trans_head.stAssistHead.cFrom = 0;
    
    m_trans_head.stAssistHead.wInfoLen = 2+4+4+1+m_trans_head.stAssistHead.cAppPassWDLen+1+1+1+m_trans_head.stAssistHead.cDeviceNoLen+1
                                            +m_trans_head.stAssistHead.cDeviceDescLen+2+2+4+1;
    
    printf("The head length of stAssist is %d\n",m_trans_head.stAssistHead.wInfoLen);
    
    return true;
}


bool DataResolution::InitTransPkgHeadEx(){
    m_trans_head.stTransExHead.wExVer = 1;
    m_trans_head.stTransExHead.wExLen = 0;
    return true;
}

bool DataResolution::InitDBPackHead(){
    
    m_trans_head.stDbHead.wVersion = 1;
    m_trans_head.stDbHead.wSubVersion = 1;
    m_trans_head.stDbHead.dwCommand = 1;
    m_trans_head.stDbHead.wSeqNum = 1;
    m_trans_head.stDbHead.dwFlag = 1;
    
    m_trans_head.stDbHead.wHeadLen = sizeof(m_trans_head.stDbHead)+m_trans_head.stAssistHead.wInfoLen+4;
    printf("The head length of m_trans_head is %d\n",m_trans_head.stDbHead.wHeadLen);
    
    m_trans_head.stDbHead.dwLength = m_trans_head.stDbHead.wHeadLen;
    return true;
}

int DataResolution::pack(){
    char *pCur = NULL;
    int piPkgLen = 1024;
    pCur = sPkgBuf;
    if(AddTransPkgReqHead_V06(&pCur, &piPkgLen, &m_trans_head)) return -1;
    
    printf("The all length is %d\n",piPkgLen);
    
    printf("The head is %s",sPkgBuf);
    sPkgBufLen = piPkgLen;
    return 0;
}

int DataResolution::send(){
    return 0;
}

int DataResolution::pack(char *p, TransPkgReqHead *pstReq){
    
    return 0;
}

int DataResolution::send(char *p, int pkgLen){
    return 0;
}

DataResolution::DataResolution(){
    memset(&m_trans_head, 0, sizeof(m_trans_head));
    memset(&sPkgBuf, 0, sizeof(sPkgBuf));
    sPkgBufLen = 0;
}

DataResolution::~DataResolution(){

}