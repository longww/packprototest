//
//  DataBuilder.h
//  testlib
//
//  Created by longw on 15/1/13.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#ifndef __testlib__DataBuilder__
#define __testlib__DataBuilder__

#include <stdio.h>
#include <stdlib.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include "SendingDataBuilder.h"
#include "ConfigDefine.h"


class DataResolution{

public:
    DataResolution();
    ~DataResolution();
    
    bool ParseDataFromObject(uint16_t cmd, char *in_pPkg, int in_iLen);

    
    bool InitAssistInfo();
    bool InitTransPkgHeadEx();
    bool InitDBPackHead();
    bool InitTransPkgReqHead();

    int pack(char *p, TransPkgReqHead *pstReq);
    
    int send(char *p, int pkgLen);
    
    int pack();
    int send();
    
private:
    TransPkgReqHead m_trans_head;
    char sPkgBuf[2048];
    int sPkgBufLen;
};

#endif /* defined(__testlib__DataBuilder__) */
