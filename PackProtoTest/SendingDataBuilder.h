//
//  SendingDataBuilder.h
//  testlib
//
//  Created by longw on 15/1/12.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#ifndef __testlib__SendingDataBuilder__
#define __testlib__SendingDataBuilder__

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    uint32_t dwLength;	//整包长度
    uint16_t wHeadLen;	//包头长度
    uint16_t wVersion;	//主版本号
    uint16_t wSubVersion;	//次版本号
    uint32_t dwCommand;	//外部命令字
    uint16_t wSeqNum;	//包序列号
    uint16_t dwFlag;	//标志位
}DBPackHead;

typedef struct {
    uint16_t wInfoLen;	//AssistInfo包头长度
    uint32_t dwUserId;	//开发者ID
    uint32_t dwAppId;			//AppID
    uint8_t cAppPassWDLen;			//App Key 密码长度
    uint8_t cAppPassWD[64];	//App密码
    uint8_t cNetworkType;	//网络类型
    uint8_t cDeviceType;	//设备类型
    uint8_t cDeviceNoLen;	//设备ID长度
    uint8_t cDeviceNo[64];	//设备ID
    uint8_t cDeviceDescLen;	//设备描述长度
    uint8_t cDeviceDesc[64];	//设备描述
    uint16_t wLocalId;	//位置ID
    uint16_t wTimeZone;	//时区
    uint32_t dwTime;	//时间
    uint8_t cFrom;		//来源
}AssistInfo;


typedef struct {
    uint16_t wExVer;
    uint16_t wExLen;		//扩展字段长度
    uint8_t cReserved[128];	//扩展字段
}TransPkgHeadEx;

//请求头
typedef struct {
    DBPackHead stDbHead;
    AssistInfo stAssistHead;
    TransPkgHeadEx stTransExHead;
    uint8_t data[64];		//包体
}TransPkgReqHead;

//应答头
typedef struct {
    DBPackHead stDbHead;
    TransPkgHeadEx stTransExHead;
    uint8_t data[64];		//包体
}TransPkgRspHead;

uint64_t Switch_V06(uint64_t a);
#ifndef htonll
#define htonll(x) Switch_V06((x))
#endif
#ifndef ntohll
#define ntohll(x) Switch_V06((x))
#endif

int GetINT8_V06(char **p, int *piLen, uint8_t *Value);
int AddINT8_V06(char **p, int *piLenLeft, uint8_t Value);
int GetINT16_V06(char **p, int *piLen, uint16_t *Value);
int AddINT16_V06(char **p, int *piLenLeft, uint16_t Value);
int GetINT32_V06(char **p, int *piLen, uint32_t *Value);
int AddINT32_V06(char **p, int *piLenLeft, uint32_t Value);
int GetINT64_V06(char **p, int *piLen, uint64_t *Value);
int AddINT64_V06(char **p, int *piLenLeft, uint64_t Value);

int GetINT8Array_zero_V06(char **p, int *piLen, uint8_t *Buf, int iBufLen);
int AddINT8Array_zero_V06(char **p, int *piLenLeft, uint8_t *Buf, int iBufLen);
int GetINT16Array_zero_V06(char **p, int *piLen, uint16_t *Buf, int iBufLen);
int AddINT16Array_zero_V06(char **p, int *piLenLeft, uint16_t *Buf, int iBufLen);
int GetINT32Array_zero_V06(char **p, int *piLen, uint32_t *Buf, int iBufLen);
int AddINT32Array_zero_V06(char **p, int *piLenLeft, uint32_t *Buf, int iBufLen);
int GetINT64Array_zero_V06(char **p, int *piLen, uint64_t *Buf, int iBufLen);
int AddINT64Array_zero_V06(char **p, int *piLenLeft, uint64_t *Buf, int iBufLen);

int GetINT8Array_split_V06(char **p, int *piLen, uint8_t *Buf, int iBufLen, char spliter);
int AddINT8Array_split_V06(char **p, int *piLenLeft, uint8_t *Buf, int iBufLen, char spliter);
int GetINT16Array_split_V06(char **p, int *piLen, uint16_t *Buf, int iBufLen, char spliter);
int AddINT16Array_split_V06(char **p, int *piLenLeft, uint16_t *Buf, int iBufLen, char spliter);
int GetINT32Array_split_V06(char **p, int *piLen, uint32_t *Buf, int iBufLen, char spliter);
int AddINT32Array_split_V06(char **p, int *piLenLeft, uint32_t *Buf, int iBufLen, char spliter);
int GetINT64Array_split_V06(char **p, int *piLen, uint64_t *Buf, int iBufLen, char spliter);
int AddINT64Array_split_V06(char **p, int *piLenLeft, uint64_t *Buf, int iBufLen, char spliter);

int GetINT8Array_end_V06(char **p, int *piLen, uint8_t *Buf, int iBufLen);
int AddINT8Array_end_V06(char **p, int *piLenLeft, uint8_t *Buf, int iBufLen);
int GetINT16Array_end_V06(char **p, int *piLen, uint16_t *Buf, int iBufLen);
int AddINT16Array_end_V06(char **p, int *piLenLeft, uint16_t *Buf, int iBufLen);
int GetINT32Array_end_V06(char **p, int *piLen, uint32_t *Buf, int iBufLen);
int AddINT32Array_end_V06(char **p, int *piLenLeft, uint32_t *Buf, int iBufLen);
int GetINT64Array_end_V06(char **p, int *piLen, uint64_t *Buf, int iBufLen);
int AddINT64Array_end_V06(char **p, int *piLenLeft, uint64_t *Buf, int iBufLen);

int GetDBPkgHead_V06(char **p, int *piLen, DBPackHead *pst);
int GetAssistInfo_V06(char **p, int *piLen, AssistInfo *pst);
int GetTransPkgHeadEx_V06(char **p, int *piLen, TransPkgHeadEx *pst);
int GetTransPkgReqHead_V06(char **p, int *piLen, TransPkgReqHead *pst);
int GetTransPkgRspHead_V06(char **p, int *piLen, TransPkgRspHead *pst);

int AddDBPkgHead_V06(char **p, int *piLen, DBPackHead *pst);
int AddAssistInfo_V06(char **p, int *piLen, AssistInfo *pst);
int AddTransPkgHeadEx_V06(char **p, int *piLen, TransPkgHeadEx *pst);
int AddTransPkgReqHead_V06(char **p, int *piLen, TransPkgReqHead *pst);
int AddTransPkgRspHead_V06(char **p, int *piLen, TransPkgRspHead *pst);

#endif /* defined(__testlib__SendingDataBuilder__) */
