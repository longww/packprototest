//
//  ConfigDefine.h
//  testlib
//
//  Created by longw on 15/1/13.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#ifndef testlib_ConfigDefine_h
#define testlib_ConfigDefine_h

#define USER_ID_STR     @"user_id_str"
#define APP_ID_STR      @"app_id_str"
#define APP_PASSWD_STR  @"app_passwd_str"

#define MACHINE_WORD_64 8
#define MACHINE_WORD_32 4

#define NETWORK_TYPE_WIFI 0
#define NETWORK_TYPE_3G 1
#define NETWORK_TYPE_OTHER 2

#define DEVICE_TYPE_IPHONE 0
#define DEVICE_TYPE_ANDROID 1

#endif
