//
//  MessageManager.h
//  testlib
//
//  Created by longw on 15/1/12.
//  Copyright (c) 2015年 longw. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "DataBuilder.h"


@interface MessageManager : NSObject

-(void) initManagerWithUserID : (NSString *) _userId
                    withAppID : (NSString *) _appId
                withAppPassWD : (NSString *) _appPassWD;

- (void) packTest;
@end
